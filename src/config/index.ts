const appConfig = {
  appUrl: process.env.REACT_APP_URL,

  unsplash: {
    authUrl: `${process.env.REACT_APP_UNSPLASH_AUTH_URL}/oauth`,
    apiUrl: process.env.REACT_APP_UNSPLASH_API_URL,
    clientId: process.env.REACT_APP_UNSPLASH_CLIENT_ID,
    clientSecret: process.env.REACT_APP_UNSPLASH_CLIENT_SECRET,
  },
};

export default appConfig;
