export const colors = {
  primaryColor: '#3bb300',
  white: '#fff',
  black: '#000',
  green1: '#2a8000',
  grey1: '#bfbfbf',
  red: '#d7443e',
};

export const radius = '3px';
