export enum LocalStorageKey {
  accessToken = 'accessToken',
  searchHistory = 'searchHistory',
}
