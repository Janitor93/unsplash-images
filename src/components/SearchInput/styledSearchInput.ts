import styled from 'styled-components';

export const StyledSearchInputWrapper = styled.div`
  display: flex;
  align-items: center;
  max-width: 500px;
  position: relative;
`

export const ButtonWrapper = styled.div`
  margin-left: 10px;
`
