import React, { useRef, useState } from 'react';

import { LocalStorageKey } from '../../enums';

import Input from '../Input/Input';
import Button from '../Button/Button';
import Dropdown from '../Dropdown/Dropdown';

import { StyledSearchInputWrapper, ButtonWrapper } from './styledSearchInput';

interface Props {
  handleSubmit(): void;
  onChange(value: string): void;
}

const SearchInput = ({ handleSubmit, onChange }: Props) => {
  const inputEl = useRef<HTMLInputElement>(null);
  const [searchHistory, setSearchHistory] = useState<string[]>([]);

  const getSearchHistory = () => {
    const prevHistory = localStorage.getItem(LocalStorageKey.searchHistory);
    let history: string[] = [];
    if (prevHistory) {
      history = JSON.parse(prevHistory);
    }

    return history;
  };

  const fillSearchHistory = () => {
    const history = getSearchHistory();
    const newSearchHistory = new Set([...history, inputEl.current?.value]);
    localStorage.setItem(LocalStorageKey.searchHistory, JSON.stringify(Array.from(newSearchHistory)));
  };

  const handleSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value);
    if (!e.target.value) {
      const history = getSearchHistory();
      setSearchHistory(history);
    } else {
      const filtredHistory = searchHistory.filter(item => item.includes(e.target.value));
      setSearchHistory(filtredHistory);
    }
  };

  const handleKeyUp = (e: React.KeyboardEvent) => {
    if (e.key === "Enter") {
      fillSearchHistory();
      handleSubmit();
    }
  };

  const onClickSubmit = () => {
    fillSearchHistory();
    handleSubmit();
  };

  const onFocused = () => {
    const history = getSearchHistory();
    const filtredHistory = history.filter(item => item.includes(inputEl.current?.value || ''));
    setSearchHistory(filtredHistory);
  };

  const onBlured = () => {
    setSearchHistory([]);
  };

  const handlePickItem = (value: string) => {
    if (inputEl.current) {
      inputEl.current.value = value;
      onChange(value);
      onBlured();
    }
  };

  return (
    <StyledSearchInputWrapper>
      <Input
        inputRef={inputEl}
        placeholder="What would you like to find?"
        onChange={handleSearchInput}
        onFocus={onFocused}
        onKeyUp={handleKeyUp}
      />
      {!!searchHistory.length && <Dropdown items={searchHistory.slice(0, 5)} pickItem={handlePickItem} />}
      <ButtonWrapper>
        <Button title="Search" disabled={!inputEl.current?.value} onClick={onClickSubmit} />
      </ButtonWrapper>
    </StyledSearchInputWrapper>
  );
}

export default SearchInput;
