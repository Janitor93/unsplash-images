import styled from 'styled-components';

export const StyledImageWrapper = styled.div`
  position: relative;
  height: 250px;
  width: 250px;
`;

export const StyledImage = styled.img`
  object-fit: cover;
  height: 250px;
  width: 250px;
`;

export const IconWrapper = styled.div`
  position: absolute;
  top: 15px;
  left: 15px;
  cursor: pointer;
`;
