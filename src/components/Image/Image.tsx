import LazyLoad from 'react-lazyload';

import Loader from '../Loader/Loader';
import LikeIcon from '../icons/LikeIcon/LikeIcon';

import { StyledImageWrapper, StyledImage, IconWrapper } from './styledImage';

interface Props {
  id: string;
  src: string;
  liked?: boolean;
  isLoggedin?: boolean;

  handleLike(id: string, liked: boolean): void;
}

const Image = ({ id, src, liked = false, isLoggedin, handleLike }: Props) => {
  const handleClick = () => handleLike(id, liked);

  return (
    <LazyLoad height={200} placeholder={<Loader />}>
      <StyledImageWrapper>
        <StyledImage src={src} />
        {isLoggedin && (
          <IconWrapper onClick={handleClick}>
            <LikeIcon liked={liked} />
          </IconWrapper>
        )}
      </StyledImageWrapper>
    </LazyLoad>
  );
};

export default Image;
