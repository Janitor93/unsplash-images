// components
import Button from '../Button/Button';

// styles
import { StyledHeader, HeaderContentWrapper, StyledTitle, ButtonWrapper } from './styledHeader';

interface Props {
  title?: string;
  isLoggedin: boolean;
  username?: string;

  handleLogin(): void;
}

const Header = ({ title = 'My app', isLoggedin, username = 'User', handleLogin }: Props) => (
  <StyledHeader>
    <HeaderContentWrapper>
      <StyledTitle>{title}</StyledTitle>
      {isLoggedin ?
        `Hi, ${username}` :
        <ButtonWrapper>
          <Button title="Log in" onClick={handleLogin} />
        </ButtonWrapper>
      }
    </HeaderContentWrapper>
  </StyledHeader>
);

export default Header;
