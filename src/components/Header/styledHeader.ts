import styled from 'styled-components';

export const StyledHeader = styled.header`
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const HeaderContentWrapper = styled.div`
  max-width: 700px;
  flex: 1;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const StyledTitle = styled.p``;

export const ButtonWrapper = styled.div`
  width: 80px;
`;
