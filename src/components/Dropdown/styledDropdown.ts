import styled from 'styled-components';

import { colors, radius } from '../../styles/constants';

export const DropdownWrapper = styled.div`
  position: absolute;
  border: 1px solid ${colors.primaryColor};
  bottom: 0;
  transform: translateY(110%);
  width: 100%;
  z-index: 10;
  border-radius: ${radius};
  background: ${colors.white};
`;
