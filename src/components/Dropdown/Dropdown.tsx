import DropdownItem from './DropdownItem/DropdownItem';

import { DropdownWrapper } from './styledDropdown';

interface Props {
  items: string[];

  pickItem(value: string): void;
}

const Dropdown = ({ items, pickItem }: Props) => (
  <DropdownWrapper>
    {items.map(item => <DropdownItem key={item} title={item} pickItem={pickItem} />)}
  </DropdownWrapper>
);

export default Dropdown;
