import { DropdownItemWrapper } from './styledDropdownItem';

interface Props {
  title: string;

  pickItem(value: string): void;
}

const DropdownItem = ({ title, pickItem }: Props) => {
  const handleClick = () => pickItem(title);
  return <DropdownItemWrapper onClick={handleClick}>{title}</DropdownItemWrapper>;
};

export default DropdownItem;
