import styled from 'styled-components';

import { colors } from '../../../styles/constants';

export const DropdownItemWrapper = styled.p`
  cursor: pointer;
  margin: 10px;
  text-align: left;
  
  &:hover {
    color: ${colors.primaryColor};
  }
`;
