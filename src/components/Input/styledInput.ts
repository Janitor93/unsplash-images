import styled from 'styled-components';

import { colors as c, radius as r } from '../../styles/constants';

export const StyledInput = styled.input`
  height: 30px;
  width: 100%;
  border: 1px solid ${c.primaryColor};
  outline: none;
  border-radius: ${r};
  transition: all 0.5s;
  padding: 0 5px;

  &:focus {
    box-shadow: 0 0 4px ${c.primaryColor};
  }
`;