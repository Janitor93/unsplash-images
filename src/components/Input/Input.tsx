import React from 'react';

import { StyledInput } from './styledInput';

export interface Props {
  placeholder?: string;
  inputRef?: React.RefObject<HTMLInputElement>;

  onChange(e: React.ChangeEvent<HTMLInputElement>): void;
  onFocus(): void;
  onKeyUp(e: React.KeyboardEvent): void;
}

const Input = ({ placeholder, inputRef, onChange, onFocus, onKeyUp }: Props) => (
  <StyledInput
    placeholder={placeholder}
    ref={inputRef}
    onChange={onChange}
    onKeyUp={onKeyUp}
    onFocus={onFocus}
  />
);

export default Input;
