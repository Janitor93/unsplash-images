import React from 'react';

import Image from '../Image/Image';

import { Grid } from './styledImageGrid';

interface Props {
  children: React.ReactNode;
}

const ImageGrid = ({ children }: Props) => <Grid>{children}</Grid>;

ImageGrid.Image = Image;

export default ImageGrid;
