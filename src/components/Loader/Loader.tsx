import { StyledSpinner } from './styledLoader';

const Loading = () => <StyledSpinner />;

export default Loading;
