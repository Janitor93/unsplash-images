import styled, { css } from 'styled-components';

import { colors as c, radius as r } from '../../styles/constants';

export const StyledButton = styled.button`
  height: 30px;
  width: 100%;
  border-radius: ${r};
  color: ${c.white};
  border: 0;
  cursor: pointer;
  background-color: ${c.primaryColor};
  transition: all 0.5s;

  &:hover {
    box-shadow: 0 0 4px ${c.primaryColor};
  }

  ${prop => prop.disabled && css`
    background-color: ${c.grey1};

    &:hover {
      box-shadow: none;
    }
  `}
`;
