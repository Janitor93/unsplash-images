import { StyledButton } from './styledButton';

export interface Props {
  disabled?: boolean;
  title: string;

  onClick(): void;
}

const Button = ({ disabled = false, title, onClick }: Props) => (
  <StyledButton disabled={disabled} onClick={onClick}>
    {title}
  </StyledButton>
);

export default Button;
