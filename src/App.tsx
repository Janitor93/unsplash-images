import { StyledApp } from './AppStyled';
import Main from './pages/Main/Main';

function App() {
  return (
    <StyledApp>
      <Main />
    </StyledApp>
  );
}

export default App;
