import { useState, useEffect } from 'react';
import InfiniteScroll from 'react-infinite-scroller';

// api
import { getPhotos, likePhoto, unlikePhoto } from '../../services/images';
import { authorize, tokenize, me } from '../../services/auth';

// enums
import { LocalStorageKey } from '../../enums';

// components
import Header from '../../components/Header/Header';
import SearchInput from '../../components/SearchInput/SearchInput';
import ImageGrid from '../../components/ImageGrid/ImageGrid';

// styles
import { MainWrapper, SearchSection, Section } from './styledMain';

interface Image {
  [key: string]: {
    id: string;
    liked: boolean;
    src: string;
  }
}

interface ImageResponse {
  id: string;
  liked_by_user: boolean;
  urls: { small: string };
}

const Main = () => {
  const [username, setUsername] = useState('');
  const [isLoggedin, setIsLoggedin] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [images, setImages] = useState<Image>({});
  const [hasMore, setHasMore] = useState(true);

  const handleChange = (value: string) => setSearchTerm(value);

  const fetchImages = async (page: number = 1) => {
    try {
      if (page === 1) {
        setImages({});
      }
      const result = await getPhotos(searchTerm, page);
      const newImages: Image = {};
      result.data.results
        .forEach((image: ImageResponse) => {
          newImages[image.id] = {
            id: image.id,
            liked: image.liked_by_user,
            src: image.urls.small,
          };
        });

      if (page >= result.data.total_pages) {
        setHasMore(false);
      }
  
      if (page === 1) {
        setImages(newImages);
      } else {
        setImages({...images, ...newImages});
      }
    } catch (error) {
      console.error(error.message);
    }
  };

  const handleLike = async (id: string, liked: boolean) => {
    try {
      if (liked) {
        await unlikePhoto(id);
      } else {
        await likePhoto(id);
      }
      const newImages = { ...images };
      newImages[id].liked = !newImages[id].liked;
      setImages(newImages);
    } catch (error) {
      console.error(error.message);
    }
  };

  const handleLogin = authorize;

  useEffect(() => {
    const token = localStorage.getItem(LocalStorageKey.accessToken);

    const fetchUserInfo = async () => {
      try {
        const result = await me();
        setUsername(result.data.first_name || result.data.username);
        setIsLoggedin(true);
      } catch (error) {
        console.error(error.message);
      }
    };

    const fetchAccessToken = async () => {
      try {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const code = urlSearchParams.get('code') as string;
        if (code) {
          await tokenize(code);
          await fetchUserInfo();
        }
      } catch (error) {
        console.error(error.message);
      }
    };

    if (token) {
      fetchUserInfo();
    } else {
      fetchAccessToken();
    }
  }, []);

  const renderImages = () => {
    const values = Object.values(images);
    if (values.length) {
      return (
        <Section>
          <InfiniteScroll
            pageStart={1}
            loadMore={page => fetchImages(page)}
            hasMore={hasMore}
          >
            <ImageGrid>
              {values.map(({ id, src, liked }) =>
                <ImageGrid.Image
                  key={id}
                  id={id}
                  src={src}
                  liked={liked}
                  handleLike={handleLike}
                  isLoggedin={isLoggedin}
                />)
              }
            </ImageGrid>
          </InfiniteScroll>
        </Section>
      );
    }
    return null;
  };

  return (
    <>
      <Header isLoggedin={isLoggedin} username={username} handleLogin={handleLogin} />
      <MainWrapper>
        <Section>
          <SearchSection>
            <SearchInput onChange={handleChange} handleSubmit={fetchImages} />
          </SearchSection>
        </Section>
        {renderImages()}
      </MainWrapper>
    </>
  )
};

export default Main;
