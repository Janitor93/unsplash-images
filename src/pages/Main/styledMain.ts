import styled from 'styled-components';

export const MainWrapper = styled.div`
  height: 100%;
`;

export const Section = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 20px 0;
`;

export const SearchSection = styled.div`
  flex: 1;
  max-width: 500px;
`;
