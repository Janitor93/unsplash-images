import axios from 'axios';

import { LocalStorageKey } from '../enums';
import appConfig from '../config';

const axiosInstance = axios.create({
  baseURL: appConfig.unsplash.apiUrl,
  headers: { 'Content-Type': 'application/json' },
});

axiosInstance.interceptors.request.use(
  (config) => {
    const accessToken = localStorage.getItem(LocalStorageKey.accessToken);
    if (accessToken) {
      config.headers.Authorization = `Bearer ${accessToken}`;
    }
    return config;
  }
);

export function apiPost(path: string, body?: object) {
  return axiosInstance.post(path, JSON.stringify(body));
}

export function apiGet(path: string, params?: object) {
  return axiosInstance.get(path, params);
}

export function apiDelete(path: string) {
  return axiosInstance.delete(path);
}
