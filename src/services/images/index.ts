import appConfig from '../../config';
import { apiPost, apiGet, apiDelete } from '../api';

export const getPhotos = (searchTerm: string, page: number = 1, perPage: number = 9) => {
  return apiGet('/search/photos', {
    params: {
      query: searchTerm,
      page,
      client_id: appConfig.unsplash.clientId,
      ...(perPage) && { per_page: perPage },
    },
  });
};

export const likePhoto = (id: string) => {
  return apiPost(`/photos/${id}/like`);
};

export const unlikePhoto = (id: string) => {
  return apiDelete(`/photos/${id}/like`);
};
