import axios from 'axios';

import { LocalStorageKey } from '../../enums';
import appConfig from '../../config';
import { apiGet } from '../api';

export const authorize = () => {
  window.location.href = `${appConfig.unsplash.authUrl}/authorize?client_id=${appConfig.unsplash.clientId}&redirect_uri=${appConfig.appUrl}&response_type=code&scope=public+write_likes`;
};

export const tokenize = async (code: string) => {
  const response = await axios.post(`${appConfig.unsplash.authUrl}/token`, {
    client_id: appConfig.unsplash.clientId,
    client_secret: appConfig.unsplash.clientSecret,
    redirect_uri: appConfig.appUrl,
    code,
    grant_type: 'authorization_code',
  });
  if (response.status === 200) {
    localStorage.setItem(LocalStorageKey.accessToken, response.data.access_token);
  }
};

export const me = () => {
  return apiGet('/me');
};
